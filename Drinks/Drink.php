<?php

namespace Drinks;

use Drinks\Behaviors\CookingBehavior;

/**
 * Class Drink
 * @package Drinks
 *
 * @property string description
 * @property string preparation
 */
abstract class Drink
{

    protected $description = "";
    protected $preparation = "";

    /** @var  CookingBehavior */
    protected $cookingBehavior;


    /**
     * Get The Description.
     **
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get The Preparation.
     **
     * @return string
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Get The Total Cost.
     **
     * @return double
     */
    public abstract function getCost();


    /**
     * Set Cookie Behavior.
     *
     * @param CookingBehavior $cb
     *
     * @return void
     */
    public function setCookingBehavior(CookingBehavior $cb)
    {
        $this->cookingBehavior = $cb;
    }

    /**
     * Execute Cook() method in behavior.
     **
     * @return void
     */
    public function performCookingBehavior()
    {
        $this->preparation = $this->cookingBehavior->cook();
    }

}
