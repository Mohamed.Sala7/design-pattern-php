<?php

namespace Drinks\Behaviors;

interface CookingBehavior{

    /**
     * Get The Cost Method.
     **
     * @return string
     */
    public function cook();

}

