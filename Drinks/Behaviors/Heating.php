<?php

namespace Drinks\Behaviors;

class Heating implements CookingBehavior{

    public function cook(){
        return(", Preparation method : heat ");
    }

}

