<?php

namespace Drinks\Behaviors;

class Cooling implements CookingBehavior{

    public function cook(){
        return(", Preparation method : cool ");
    }

}

