<?php

namespace Drinks;

use Drinks\Behaviors\Heating;

/**
 * Class Coffe
 * @package Drinks
 *
 * @property double cost
 */

class Coffee extends Drink
{
    private $cost = 20;

    public function __construct($name)
    {
        $this->description = $name;
        $this->cookingBehavior = new Heating();
    }

    /**
     * Get The Cost.
     **
     * @return double
     */
    public function getCost()
    {
        return $this->cost;
    }
}
