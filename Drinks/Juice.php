<?php

namespace Drinks;

use Drinks\Behaviors\Cooling;

/**
 * Class Juice
 * @package Drinks
 *
 * @property double cost
 */

class Juice extends Drink
{
    private $cost = 10;

    public function __construct($name)
    {
        $this->description = $name;
        $this->cookingBehavior = new Cooling();
    }

    /**
     * Get The Cost.
     **
     * @return double
     */
    public function getCost()
    {
        return $this->cost;
    }
}
