<?php

namespace Extras;

class Mocha extends ExtraDecorator{


    /**
     * @inheritDoc
     */
    public function getCost()
    {
        return 3 + $this->drink->getCost();
    }

    /**
     * @inheritDoc
     * override main class
    */
    public function getDescription()
    {
        return parent::getDescription() . ", Mocha";
    }

}
