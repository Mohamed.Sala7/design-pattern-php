<?php

namespace Extras;

class Milk extends ExtraDecorator{


    /**
     * @inheritDoc
     */
    public function getCost()
    {
        return 5 + $this->drink->getCost();
    }

    /**
     * @inheritDoc
     * override main class
    */
    public function getDescription()
    {
        return parent::getDescription() . ", Milk";
    }

}
