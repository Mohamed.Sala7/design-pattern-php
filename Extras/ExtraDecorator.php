<?php

namespace Extras;
use Drinks\Drink;

abstract class ExtraDecorator extends Drink{

    /** @var  Drink */
    protected $drink;

    public function __construct(Drink $drink)
    {
        $this->drink = $drink;
    }

    /**
     * @inheritDoc
     * override main class
     */
    public function getDescription()
    {
        return $this->drink->getDescription();
    }

    /**
     * @inheritDoc
     * override main class
     */
    public function getPreparation()
    {
        return $this->drink->getPreparation();
    }
}
