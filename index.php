<?php

namespace CoffeeShop;
spl_autoload_register(
    function ($class_name) {
        require "$class_name.php";
    }
);

use Drinks\Behaviors\Heating;
use Drinks\Coffee;
use Drinks\Juice;
use Extras\Milk;
use Extras\Mocha;
use Observers\Accountant;
use Observers\Kitchen;
use Observers\Order;


$espresso    = new Coffee("Espresso");
$orangeJuice = new Juice("Orange ");
$lemonJuice  = new Juice("Lemon ");


// ------------------ The Strategy Pattern ------------------ //
$espresso->performCookingBehavior();
$orangeJuice->performCookingBehavior();
$lemonJuice->performCookingBehavior();


// ------------------ The Decorator Pattern ------------------ //
echo($espresso->getDescription() . ": $" . $espresso->getCost() . "<br>");

$orangeJuice = new Milk($orangeJuice);
$orangeJuice = new Mocha($orangeJuice);
echo($orangeJuice->getDescription() . ": $" . $orangeJuice->getCost() . "<br>");


echo("************************************************************* <br>");
// ------------------ The Observer Pattern ------------------ //
$orderData = new Order();

$accountant        = new Accountant($orderData);
$generalAccountant = new Accountant($orderData);
$kitchen           = Kitchen::getInstance($orderData); // ----------- Singleton -------------

$orderData->setOrder($espresso);
$orderData->setOrder($orangeJuice);

echo("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ <br>");
// ------------------ The Strategy Pattern ------------------ //
$orderData->setOrder($lemonJuice);
$lemonJuice->setCookingBehavior(new Heating());
$lemonJuice->performCookingBehavior();
$orderData->setOrder($lemonJuice);




