<?php

namespace Observers;

use Drinks\Drink;


/**
 * Class Order
 *
 * @property Kitchen uniqueInstance
 * @property Subject orderData
 * @property Drink $drink
 */
class Kitchen implements Observer
{

    private static $uniqueInstance = null;

    private $orderData;
    private $drink;

    private function __construct(Subject $orderData)
    {
        $this->orderData = $orderData;
        $this->orderData->registerObserver($this);
    }

    public static function getInstance(Subject $orderData)
    {
        if (self::$uniqueInstance == null) {
            self::$uniqueInstance = new Kitchen($orderData);
        }
        return self::$uniqueInstance;
    }


    public function update(Drink $drink)
    {
        $this->drink = $drink;
        $this->display();
    }

    public function display()
    {
        echo("Kitchen => Description: " . $this->drink->getDescription() . "  " . $this->drink->getPreparation()."<br>");
    }

}
