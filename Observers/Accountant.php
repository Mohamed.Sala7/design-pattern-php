<?php
namespace Observers;

use Drinks\Drink;

/**
 * Class Order
 *
 * @property Subject orderData
 * @property Drink $drink
 */
class Accountant implements Observer
{
    private $orderData;
    private $drink;

    public function __construct(Subject $orderData)
    {
        $this->orderData = $orderData;
        $this->orderData->registerObserver($this);
    }

    public function update(Drink $drink)
    {
        $this->drink = $drink;
        $this->display();
    }

    public function display()
    {
        echo("Accountant => Description: " . $this->drink->getDescription() . " , Cost: " . $this->drink->getCost()."<br>");
    }

}

