<?php

namespace Observers;

use Drinks\Drink;

interface Observer
{
    public function update(Drink $drink);
}
