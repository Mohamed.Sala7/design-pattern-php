<?php

namespace Observers;

use Drinks\Drink;

/**
 * Class Order
 *
 * @property Drink drink
 * @property array<Observer> observers
 */
class Order implements Subject
{

    private $observers;
    private $drink;

    public function __construct()
    {
        $this->observers = [];
    }

    public function registerObserver(Observer $o)
    {
        $this->observers[spl_object_hash($o)] = $o;
    }

    public function removeObserver(Observer $o)
    {
        unset($this->observers[spl_object_hash($o)]);
    }

    public function setOrder(Drink $drink)
    {
        $this->drink = $drink;
        $this->orderCreated();
    }

    public function orderCreated()
    {
        $this->notifyObservers();
    }

    public function notifyObservers()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this->drink);
        }
    }
}
